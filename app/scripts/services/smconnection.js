'use strict';

/**
 * @ngdoc service
 * @name smUserModule.SmConnection
 * @description
 * # SmConnection
 * Factory in the smUserModule.
 */
angular.module('smUserModule').factory('SmConnection',['$q', '$http', 'SmSettings', 'SmUser', '$httpParamSerializer', function ($q, $http, SmSettings, SmUser, $httpParamSerializer) {
    // Public API here
    return {
        pendingSearch : null, 
        cancelSearch : angular.noop,
        cachedQuery : null,
        lastSearch : null,
        
        allContacts : null,
        contacts : [],

        selected : [],
        /**
         * Async search for contacts
         * Also debounce the queries; since the md-contact-chips does not support this
         */
        addSelected : function (selected) {
            this.selected = selected;
        },
        searchConnections: function (criteria) {
            var root = this;
            root.cachedQuery = criteria;
            root.deferred = $q.defer();
            if (!root.pendingSearch || !root.debounceSearch()) {
                $http({
                    url     : SmSettings.API_HOST + '/user/search',
                    method  : 'GET',
                    params  : {'key' : criteria},
                    paramSerializer: '$httpParamSerializerJQLike'})
                    .success(root.searchSuccess.bind(root))
                    .error(root.searchError.bind(root));
                return root.deferred.promise;
            }
            return pendingSearch;
        },
        searchSuccess : function (response) {
            this.deferred.resolve(response);
            this.refreshDebounce();
        },
        searchError : function (error) {
            this.deferred.reject(error);
        },
        refreshDebounce: function () {
            var root = this;
            root.lastSearch = 0;
            root.pendingSearch = null;
            root.cancelSearch = angular.noop;
        },

        /**
         * Debounce if querying faster than 300ms
         */
        debounceSearch: function () {
            var root = this;
            var now = new Date().getMilliseconds();
            root.lastSearch = root.lastSearch || now;

            return ((now - lastSearch) < 300);
        },
        
        update : function (method, model1Name, model1Id, model2Name, model2Id) {
            var root = this, data = {};
            if (model1Name && model1Id) data[model1Name] = model1Id;
            if (model2Name && model2Id) data[model2Name] = model2Id;

            $http({
                url: SmSettings.API_HOST + '/connect?' + $httpParamSerializer(data),
                method: method
            })
                .success(root.updateSuccess.bind(root))
                .error(root.updateError.bind(root));
        },
        updateSuccess : function (response) {
            // console.dir(response);
        },
        updateError : function (error) {
            // console.dir(error);
        },
        compareNewSelected : function (newValSrc, oldValSrc) {
            var newVal, oldVal, response;
            newVal = angular.copy(newValSrc);
            oldVal = angular.copy(oldValSrc);

            angular.forEach(newVal, function(value1, key1) {
                angular.forEach(oldVal, function(value2, key2) {
                    if (value1.id === value2.id ) {
                        delete newVal[key1];
                        delete oldVal[key2];
                    }
                });
            });

            if (newVal.length > 0) {
                angular.forEach(newVal, function (value, key) {
                    response = {
                        method: 'POST',
                        id: value.id,
                    }
                })
            }
            if (oldVal.length > 0) {
                angular.forEach(oldVal, function (value, key) {
                    response = {
                        method: 'DELETE',
                        id: value.id,
                    }
                })
            }
            return response;
        }
    };
}]);



