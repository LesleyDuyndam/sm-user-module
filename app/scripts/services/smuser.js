'use strict';

/**
 * @ngdoc service
 * @name smUserModuleApp.SmUser
 * @description
 * # SmUser
 * Factory in the smUserModuleApp.
 */
angular.module('smUserModule').factory('SmUser',['$http', '$cookies', '$q', 'SmSettings', function ($http, $cookies, $q, SmSettings) {
    return {
        ID          : 0,
        ROLE        : 0,
        asyncContacts : [],
        loginPending : true,
        deferred    : undefined,
        profile     : {},
        collection  : {},
        credentials : {
            email: '',
            password: ''
        },
        // Get user key. not found = null
        getLoginStatus: function (key) {
            this.id;
        },
        getProfile: function (key) {
            var root = this;
            if (key) {
                if (!root.has('profile', key)) {
                    return false
                }
                if (root.has('profile', key)) {
                    return root.profile[key];
                }
            }
            return root.profile;
        },
        // Get user key. not found = null
        getCollection: function (key) {
            var root = this;
            if (key) {
                if (!root.has('collection', key)) {
                    return false
                }
                if (root.has('collection', key)) {
                    return root.collection[key];
                }
            }
            return root.collection;
        },
        // Boolean if user key exists
        has: function (objectName, key) {
            var root = this;
            return root[objectName][key] !== undefined && root[objectName][key] !== '';
        },
        // Check if the user has permission role; 0 = guest, 1 = member, 2 = admin, 3 = master
        is: function (role, direction) {
            var root = this;
            role = role == undefined ? 0 : role;
            direction = direction == undefined ? false : direction;
            switch(direction){
                case false:
                    return root.ROLE === role;
                    break;
                case 'or-higher':
                    return root.ROLE >= role;
                    break;
                case 'or-lower':
                    return root.ROLE <= role;
                    break;
            }
            return false;
        },
        isGuest: function (direction) {
            var root = this;
            return root.is(0, direction);
        },
        isMember: function (direction) {
            var root = this;
            return root.is(1, direction);
        },
        isAdmin: function (direction) {
            var root = this;
            return root.is(2, direction);
        },
        isMaster: function (direction) {
            var root = this;
            return root.is(3, direction);
        },
        // If the user submits the form, try to login user
        login: function () {
            var root = this;
            root.deferred = $q.defer();
            $http({
                url     : SmSettings.API_HOST + '/user/login',
                method  : 'GET',
                params  : root.credentials,
                paramSerializer: '$httpParamSerializerJQLike'})
                .success(root.handleLoginSuccess.bind(root))
                .error(root.handleLoginError.bind(root));
            return root.deferred.promise;
        },
        handleLoginSuccess: function (api_token) {
            var root = this;
            SmSettings.API_TOKEN  = api_token;
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + api_token;
            $cookies.put('SM_API_TOKEN', api_token);
            root.getModel();
            root.deferred.resolve();
        },
        handleLoginError: function (response) {
            var root = this;
            root.loginPending = false;
            root.profile = null;
            root.deferred.reject(response);
        },
        // If the user submits the form, try to login user
        logout: function () {
            var root = this;
            root.profile = {};
            root.ID = 0;
            root.ROLE = 0;
            root.loginPending = false;
            $cookies.remove('SM_API_TOKEN');
        },
        // If a SM_API_TOKEN is found in the cookies, try to login user
        getModel: function () {
            var root = this;
            root.deferred = $q.defer();
            $http({
                url     : SmSettings.API_HOST + '/user/getModel',
                method  : 'GET',
                paramSerializer: '$httpParamSerializerJQLike'})
                .success(root.handleModelSuccess.bind(root))
                .error(root.handleModelError.bind(root));
        },
        // If login responded with success, handle further actions
        handleModelSuccess: function (response) {
            var root = this;
            root.logged     = true;
            root.ID         = response.id;
            root.ROLE       = response.role;
            root.profile    = response.profile;
            root.collection = root.map(response);
            root.deferred.resolve(root);
        },
        // If login responded with errors, handle errors properly
        handleModelError: function (response) {
            var root = this;
            root.deferred.reject(response);
        },
        map : function (object) {
            var buffer = {};
            for (var key in object) {
                if (!angular.isArray(object[key])) continue;
                buffer[key] = object[key];
            }
            return buffer;
        }
    };
}]);