'use strict';

/**
 * @ngdoc overview
 * @name smUserModule
 * @description
 * # smUserModule
 *
 * Main module of the application.
 */
angular
    .module('smUserModule', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial'
    ])

    .factory('SmSettings',['$cookies', function ($cookies) {
        return {
            API_HOST     : SM.API_HOST,
            API_TOKEN    : $cookies.get('SM_API_TOKEN'),
            APP_HOST     : SM.APP_HOST,
            APP_TOKEN    : SM.APP_TOKEN,
        };
    }])

    // Set $http auth logic
    .run(function($rootScope, $http, $cookies, SmUser, SmSettings) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + SmSettings.API_TOKEN;

        $rootScope.$watch(function() {
            return $cookies.SM_API_TOKEN;
        }, function() {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + SmSettings.API_TOKEN;

            // If token is set, get the model
            if (SmSettings.API_TOKEN !== undefined)
            {
                SmUser.getModel();
            } else {
                SmUser.logout();
            }
            // else
            // {
            //     SmUser.logout();
            // }
        });
    })

    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
    });
