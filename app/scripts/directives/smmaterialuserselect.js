'use strict';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smMaterialUserSelect
 * @description
 * # smMaterialUserSelect
 */
angular.module('smUserModule').directive('smMaterialUserSelect',['SmConnection', function (SmConnection) {
    return {
      template: '' +
      '<div class="select user" ng-hide="hasInitialized">' +
      '     <md-contact-chips ng-model="smModel" md-contacts="SmConnection.searchConnections($query)" md-contact-name="name" md-contact-image="image" md-contact-email="email" md-require-match="true" placeholder="To"></md-contact-chips> ' +
      '</div>',
      restrict: 'A',
        scope: {
            smModel: '='
        },
      link: function postLink(scope, element, attrs) {
          // SmConnection.update('POST', 'User', 40, 'Organisation', 10);
          scope.hasInitialized = 2;
            scope.SmConnection = SmConnection;
            scope.$watch('smModel', function (newVal, oldVal) {
                if (scope.hasInitialized) {
                    scope.hasInitialized--;
                    return false;
                }

                if (scope.smModel == undefined) return false;
                var tempUser = SmConnection.compareNewSelected(newVal, oldVal);
                console.dir(tempUser);
                if (!tempUser) return false;
                SmConnection.update(tempUser.method, 'User', tempUser.id, attrs.smModelName, attrs.smModelId);
            }, true);
      }
    };
  }]);
