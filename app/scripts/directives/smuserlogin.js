'use strict';

var NG_HIDE_CLASS = 'ng-hide';
var NG_HIDE_IN_PROGRESS_CLASS = 'ng-hide-animate';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smUserLogin
 * @description
 * # smUserLogin
 */
angular.module('smUserModule').directive('smUserLogin', ['SmUser', '$animate',function (SmUser, $animate) {
        return {
            restrict: 'A',
            template: '' +
            '<form name="loginForm" ng-submit="login()" ng-hide="SmUser.isMember(\'or-higher\')">' +
            '<fieldset class="email">' +
            '<label for="email">E-mailadres</label>' +
            '<input type="text" name="email" id="email" ng-model="credentials.email">' +
            '</fieldset>' +
            '<fieldset class="password">' +
            '<label for="password">Wachtwoord</label>' +
            '<input type="password" name="password" id="password" ng-model="credentials.password">' +
            '</fieldset>' +
            '<fieldset class="submit">' +
            '<input type="submit" value="Login">' +
            '</fieldset>' +
            '</form>',
            link: function postLink(scope, element, attrs) {
                scope.$watch(
                    function () {
                        return SmUser.getProfile();
                    },
                    function (newVal, oldVal) {
                        $animate[SmUser.isMember('or-higher') ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                    }
                );
                scope.credentials = SmUser.credentials;
                scope.login = function () {
                    SmUser.login();
                }
            }
        };
    }]);
