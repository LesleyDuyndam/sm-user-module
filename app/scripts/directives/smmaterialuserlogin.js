'use strict';

var NG_HIDE_CLASS = 'ng-hide';
var NG_HIDE_IN_PROGRESS_CLASS = 'ng-hide-animate';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smMaterialUserLogin
 * @description
 * # smMaterialUserLogin
 */
angular.module('smUserModule').directive('smMaterialUserLogin', ['SmUser', '$animate',function (SmUser, $animate) {
    return {
        restrict: 'A',
        template: '' +
        '    <md-card>  '  +
        '           <md-card-title>  '  +
        '               <md-card-title-text>  '  +
        '                   <span class="md-headline">Login</span>  '  +
        '               </md-card-title-text>  '  +
        '           </md-card-title>  '  +
        '           <md-card-content>  '  +
        '               <form md-card name="loginForm" ng-hide="SmUser.isMember(\'or-higher\')">  '  +
        '                   <div layout-gt-sm="row">  '  +
        '                       <md-input-container class="md-block" flex-gt-sm>  '  +
        '                           <label>Email</label>  '  +
        '                           <input name="email" ng-model="credentials.email" required minlength="5" maxlength="100"/>  '  +
        '                           <div class="hint">How can we reach you?</div>  '  +
        '                           <div ng-messages="loginForm.email.$error">  '  +
        '                               <div ng-message-exp="[\'required\', \'minlength\', \'maxlength\', \'pattern\']">  '  +
        '                                   Your email must be between 10 and 100 characters long and look like an e-mail address.  '  +
        '                               </div>  '  +
        '                           </div>  '  +
        '                       </md-input-container>  '  +
        '                   </div>  '  +
        '                   <div layout-gt-sm="row">  '  +
        '                       <md-input-container class="md-block" flex-gt-sm>  '  +
        '                           <label>Password</label>  '  +
        '                           <input type="password" md-minlength="4" md-maxlength="100" required name="password" ng-model="credentials.password" />  '  +
        '                           <div class="hint">Tell us what we should call you!</div>  '  +
        '                           <div ng-messages="loginForm.password.$error">  '  +
        '                               <div ng-message="required">Password is required.</div>  '  +
        '                               <div ng-message="md-maxlength">The password has to be less than 100 characters long.</div>  '  +
        '                               <div ng-message="md-minlength">The password has to be more than 10 characters long.</div>  '  +
        '                           </div>  '  +
        '                       </md-input-container>  '  +
        '                   </div>  '  +
        '               </form>  '  +
        '           </md-card-content>  '  +
        '           <md-card-actions layout="row" layout-align="start center">  '  +
        '               <md-button class="md-raised md-primary" type="submit" ng-click="login()">Login</md-button>  '  +
        '           </md-card-actions>  '  +
        '      </md-card>  ',
    link: function postLink(scope, element, attrs) {
            scope.$watch(
                function () {
                    return SmUser.getProfile();
                },
                function (newVal, oldVal) {
                    $animate[SmUser.loginPending || SmUser.isMember('or-higher') ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                }
            );
            scope.credentials = SmUser.credentials;
            scope.login = function () {
                SmUser.login().then(function () {
                    $animate[SmUser.isMember('or-higher') ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                });
            }
        }
    };
}]);
