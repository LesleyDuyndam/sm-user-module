'use strict';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smUserLogoutButton
 * @description
 * # smUserLogoutButton
 */
angular.module('smUserModule').directive('smUserLogout',['SmUser', '$animate', function (SmUser, $animate) {
    return {
        restrict: 'A',
      template: '<md-button ng-click="logout()">Logout</md-button>',
      link: function postLink(scope, element, attrs) {
          scope.logout = function () {
              SmUser.logout();
          };
          scope.$watch(
              function () {
                  return SmUser.getProfile();
              },
              function (newVal, oldVal) {
                  $animate[SmUser.isGuest() ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});;
              }
          );
      }
    };
  }]);
