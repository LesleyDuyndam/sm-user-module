'use strict';

var NG_HIDE_CLASS = 'ng-hide';
var NG_HIDE_IN_PROGRESS_CLASS = 'ng-hide-animate';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smUserCardSmall
 * @description
 * # smUserCardSmall
 */

angular.module('smUserModule').directive('smUserCardSmall', ['SmUser', '$animate',function (SmUser, $animate) {
    return {
        restrict: 'A',
        template: '' +
        '<div>' +
        '   <div class="thumbnail-wrapper">' +
        // '       <div class="thumbnail" ng-style="{\'background-image\':\'url(\'SmUser.info.thumbnail\')\'}"></div>' +
        '       <div class="thumbnail" ng-style="{\'background-image\':\'url(\'https://api.adorable.io/avatars/120/abott@adorable.io.png\')\'}"></div>' +
        '   </div>' +
        '   <div class="info-wrapper">' +
        '       <h3 class="email" ng-bind="user.email"></h3>' +
        '       <h6 class="name" ng-bind="user.name"></h6>' +
        '   </div>' +
        '</div>',
        link: function postLink(scope, element, attrs) {
            scope.$watch(
                function () {
                    return SmUser.getProfile();
                },
                function (newVal, oldVal) {
                    $animate[SmUser.isGuest() ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                }
            );
        }
    };
}]);
