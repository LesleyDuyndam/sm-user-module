'use strict';

var NG_HIDE_CLASS = 'ng-hide';
var NG_HIDE_IN_PROGRESS_CLASS = 'ng-hide-animate';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smMaterialUserAvatar
 * @description
 * # smMaterialUserAvatar
 */

angular.module('smUserModule').directive('smMaterialUserAvatar', ['SmUser', '$animate',function (SmUser, $animate) {
    return {
        restrict: 'E',
        template: '<img ng-src="{{avatar}}" class="md-user-avatar" alt="{{alt}}" />',
        link: function postLink(scope, element, attrs) {
            scope.$watch(
                function () {
                    return SmUser.getProfile();
                },
                function (newVal, oldVal) {
                    $animate[SmUser.isGuest() ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                    scope.avatar = 'https://api.adorable.io/avatars/120/abott@adorable.io.png';
                    scope.alt = 'Lesley Duyndam Profile picture';
                }
            );
        }
    };
}]);