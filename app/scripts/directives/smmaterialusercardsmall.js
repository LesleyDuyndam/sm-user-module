'use strict';

var NG_HIDE_CLASS = 'ng-hide';
var NG_HIDE_IN_PROGRESS_CLASS = 'ng-hide-animate';

/**
 * @ngdoc directive
 * @name smUserModule.directive:smMaterialUserCardSmall
 * @description
 * # smMaterialUserCardSmall
 */

angular.module('smUserModule').directive('smMaterialUserCardSmall', ['SmUser', '$animate', function (SmUser, $animate) {
    return {
        restrict: 'A',
        template: '' +
        '    <md-toolbar class="md-tall" layout="column">' +
        '       <div flex layout="row">' +
        '           <div flex></div>' +
        '           <div flex></div>' +
        '           <div flex></div>' +
        '           <md-menu-bar class="inset">' +
        '               <a ng-href="#/notifications" md-button class="md-icon-button" aria-label="Notifications">' +
        '                   <md-icon>comment</md-icon>' +
        '               </a>' +
        '               <md-menu>' +
        '                   <md-button class="md-icon-button" aria-label="More" ng-click="$mdOpenMenu()">' +
        '                       <md-icon>more_vert</md-icon>' +
        '                   </md-button>' +
        '                   <md-menu-content>' +
        '                       <md-menu-item class="md-indent">' +
        '                       <md-icon>account_circle</md-icon>' +
        '                           <a ng-href="#/settings" md-button>Profile</a>' +
        '                       </md-menu-item>' +
        '                       <md-menu-item class="md-indent">' +
        '                       <md-icon>settings</md-icon>' +
        '                           <a ng-href="#/settings" md-button>Settings</a>' +
        '                       </md-menu-item>' +
        '                       <md-menu-divider></md-menu-divider>' +
        '                       <md-menu-item class="md-indent">' +
        '                       <md-icon>power_settings_new</md-icon>' +
        '                           <md-button ng-click="logout()">logout</md-button>' +
        '                       </md-menu-item>' +
        '                   </md-menu-content>' +
        '               </md-menu>' +
        '           </md-menu-bar>' +
        '       </div>' +
        '       <span flex></span>  '  +
        '       <div layout="row" class="inset">  '  +
        '           <sm-material-user-avatar></sm-material-user-avatar> ' +
        '           <div layout="column">' +
        // '               <div class="md-title" ng-bind="user.name"></div>  '  +
        '               <div class="md-subtitle" ng-bind="user.email"></div>  ' +
        '           </div>'  +
        '       </div>  '  +
        '  </md-toolbar>  ',
        link: function postLink(scope, element, attrs) {
            scope.logout = function () {
                SmUser.logout();
            };
            scope.$watch(function () {return SmUser.getProfile()},
                function (newVal, oldVal) {
                    $animate[SmUser.isGuest() ? 'addClass' : 'removeClass'](element, NG_HIDE_CLASS, {tempClasses: NG_HIDE_IN_PROGRESS_CLASS});
                    scope.user = SmUser.getProfile();
                }
            );
        }
    };
}]);