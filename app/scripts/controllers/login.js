'use strict';

/**
 * @ngdoc function
 * @name SmUserModuleApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the SmUserModuleApp
 */
angular.module('smUserModule').controller('LoginCtrl', ['$scope', 'SmUser', 'SmConnection', function ($scope, SmUser, SmConnection) {
    $scope.SmUser = SmUser;
    $scope.SmConnection = SmConnection;
}]);
