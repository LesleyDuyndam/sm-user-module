'use strict';

/**
 * @ngdoc function
 * @name smUserModule.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the smUserModule
 */
angular.module('smUserModule')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
