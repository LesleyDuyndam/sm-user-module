'use strict';

/**
 * @ngdoc function
 * @name smUserModule.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the smUserModule
 */
angular.module('smUserModule')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
