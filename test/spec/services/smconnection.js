'use strict';

describe('Service: SmConnection', function () {

  // load the service's module
  beforeEach(module('smUserModule'));

  // instantiate service
  var SmConnection;
  beforeEach(inject(function (_SmConnection_) {
    SmConnection = _SmConnection_;
  }));

  it('should do something', function () {
    expect(!!SmConnection).toBe(true);
  });

});
