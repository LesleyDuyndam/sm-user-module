'use strict';

describe('Service: SmUser', function () {

  // load the service's module
  beforeEach(module('smUserModuleApp'));

  // instantiate service
  var SmUser;
  beforeEach(inject(function (_SmUser_) {
    SmUser = _SmUser_;
  }));

  it('should do something', function () {
    expect(!!SmUser).toBe(true);
  });

});
