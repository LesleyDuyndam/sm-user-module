'use strict';

describe('Directive: smMaterialUserAvatar', function () {

  // load the directive's module
  beforeEach(module('smUserModule'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<sm-material-user-avatar></sm-material-user-avatar>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the smMaterialUserAvatar directive');
  }));
});
